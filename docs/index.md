# GitLab Release command-line tool

> [Introduced](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/6) in GitLab 12.10.

GitLab Release command-line tool is an application written in [Golang](https://golang.org/)
to interact with [GitLab's Releases API](https://docs.gitlab.com/ee/api/releases/index.html) through the command line and through GitLab CI/CD's configuration file, [`.gitlab-ci.yml`](https://docs.gitlab.com/ee/ci/yaml/#release).
The minimum supported Go version is v1.13.

It consumes instructions in the `:release` node of the `.gitlab-ci.yml` to create a Release object in GitLab Rails.

The GitLab Release CLI is a decoupled utility that may be called by the GitLab Runner,
by a third-party CI or directly from the command line.
It uses the CI `Job-Token` to authorize against the GitLab Rails API, which is passed to it by the GitLab Runner.

The CLI can also be called independently, and can still create the Release via Rails API
if the `Job-Token` and correct command line params are provided.

```mermaid
sequenceDiagram
  participant Rails
  participant ReleaseCLI
  participant Runner
  Runner->>Rails: 1. Runner calls API for job info
  Rails->>Rails : 2. Yaml exposed as Steps
  Runner->>ReleaseCLI : 3. Runner calls CLI on Job success
  ReleaseCLI->>Rails : 4. CLI retrieves Release Steps
  ReleaseCLI->>Rails : 5. CLI creates Release
```

1. Runner calls API for job info: the Runner polls Rails for new Jobs.

1. Yaml exposed as **Steps**: The `release` node of the `.gitlab-ci.yml` configuration is converted into **Steps** and made available via API endpoint.

1. The Runner calls GitLab Release CLI: the Runner executes the job, and upon success calls the **GitLab Release**.

1. GitLab Release CLI retrieves Release **Steps**: the GitLab Release calls the Rails API to retrieve the `release` configuration (as **Steps**).

1. GitLab Release CLI creates a Release: the GitLab Release CLI makes an API call to Rails to create the new Release.

## Usage

To get started, open your project in a terminal and run `release-cli help`
for usage options. The output will be:

```plaintext
NAME:
   release-cli - A CLI tool that interacts with GitLab's Releases API

USAGE:
   help [global options] command [command options] [arguments...]

VERSION:
   0.6.0

DESCRIPTION:

CLI tool that interacts with GitLab's Releases API https://docs.gitlab.com/ee/api/releases/.

All configuration flags will default to GitLab's CI predefined environment variables (https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).
To override these values, use the [GLOBAL OPTIONS].

Get started with release-cli https://gitlab.com/gitlab-org/release-cli.

AUTHOR:
   GitLab Inc. <support@gitlab.com>

COMMANDS:
   create   Create a Release using GitLab's Releases API https://docs.gitlab.com/ee/api/releases/#create-a-release
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --server-url value                 The base URL of the GitLab instance, including protocol and port, for example https://gitlab.example.com:8080 [$CI_SERVER_URL]
   --job-token value                  Job token used for authenticating with the GitLab Releases API [$CI_JOB_TOKEN]
   --project-id value                 The current project's unique ID; used by GitLab CI internally [$CI_PROJECT_ID]
   --timeout value                    HTTP client's timeout in Go's duration format https://golang.org/pkg/time/#ParseDuration (default: 30s) [$RELEASE_CLI_TIMEOUT]
   --private-token value              Private token used for authenticating with the GitLab Releases API, requires api scope https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html, overrides job-token (default: "GctZHHGpgAN4awVasWej") [$GITLAB_PRIVATE_TOKEN]
   --additional-ca-cert-bundle value  Configure a custom SSL CA certificate authority, can a path to file or the content of the certificate [$ADDITIONAL_CA_CERT_BUNDLE]
   --insecure-https                   Set to true if you want to skip the client verifying the server's certificate chain and host name (default: false) [$INSECURE_HTTPS]
   --help, -h                         Show help (default: false)
   --version, -v                      Print the version (default: false)
```

### Create a new release

This command uses the [Create a Release](https://docs.gitlab.com/ee/api/releases/) API.

```shell
release-cli --server-url https://gitlab.com --job-token=SOME_JOB_TOKEN --project-id 12345 create help
```

The output is:

```plaintext
NAME:
   help create - Create a Release using GitLab's Releases API https://docs.gitlab.com/ee/api/releases/#create-a-release

USAGE:
   help create [command options] [arguments...]

OPTIONS:
   --name value               The release name
   --description value        The description of the release; you can use Markdown. A file can be used to read the description contents, must exist inside the working directory; if it contains any whitespace, it will be treated as a string
   --tag-name value           The tag the release will be created from [$CI_COMMIT_TAG]
   --ref value                If tag_name doesn’t exist, the release will be created from ref; it can be a commit SHA, another tag name, or a branch name [$CI_COMMIT_SHA]
   --assets-links-name value  [DEPRECATED use --assets-link instead] List of asset link names; must be used with --asset-links-url; ignored if --assets-link is defined (e.g. --assets-links-name "asset 1" --assets-links-url "https://example.com/url/1" --assets-links-name "asset 2" --assets-links-url "https://example.com/url/2")
   --assets-links-url value   [DEPRECATED use --assets-link instead] List of asset link URLs; must be used with --asset-links-name; ignored if --assets-link is defined (e.g. --assets-links-name "asset 1" --assets-links-url "https://example.com/url/1" --assets-links-name "asset 2" --assets-links-url "https://example.com/url/2")
   --assets-link value        JSON string representation of an asset link; takes precedence over --assets-links-name and --assets-links-url (e.g. --assets-link='{"name": "Asset1", "url":"https://<domain>/some/location/1", "type": "other", "filepath": "xzy" }'
   --milestone value          List of the titles of each milestone the release is associated with (e.g. --milestone "v1.0" --milestone "v1.0-rc)"; each milestone needs to exist
   --released-at value        The date when the release will be/was ready; defaults to the current time; expected in ISO 8601 format (2019-03-15T08:00:00Z)
   --help, -h                 Show help (default: false)
```

## Configuration

All configuration flags will default to [GitLab's CI predefined environment variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).

To override these values, use the flags available under the `GLOBAL OPTIONS`.
For example, use the flags to create a release with a custom GitLab server URL.

```shell
release-cli --server-url https://gitlab.mydomain.com create --name "My Release" --description "This is a new release for my amazing tool"
```

## Using this tool with a private token

To use an access token to authenticate with the API, do one of the following:

- Pass the token to the `release-cli` as the `--private-token` global flag.
- Define an environment variable `$GITLAB_PRIVATE_TOKEN` and set it to the token.

To create a private token, see [Creating a personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token).
The `api` scope is required.

For example:

```sh
release-cli --server-url https://gitlab.mydomain.com --private-token "my-private-token" create --name "My Release" --description "This is a new release for my amazing tool"
```

**WARNING**:
The `--private-token` flag overrides the `--job-token` flag.

## Using this tool in GitLab CI

The `release-cli` is available as a [Docker image](https://docs.gitlab.com/ee/ci/yaml/#release-cli-docker-image)
through the `.gitlab-ci.yml` file's [`release`](https://docs.gitlab.com/ee/ci/yaml/#release)
keyword.

If you would like to try GitLab Release on your project, add the following `script` to your `.gitlab-ci.yml` file:

```yaml
release-branch:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli
  when: manual
  # We recommend the use of `rules` to prevent these pipelines
  # from running. See the notes section below for details.
  rules:
    - if: $CI_COMMIT_TAG
      when: never

  script:
    - >
      release-cli create --name release-branch-$CI_JOB_ID --description release-branch-$CI_COMMIT_REF_NAME-$CI_JOB_ID
      --tag-name job-$CI_JOB_ID --ref $CI_COMMIT_SHA
      --assets-link '{"name":"Asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy"}'
      --assets-link '{"name":"Asset2","url":"https://<domain>/some/location/2"}'
      --milestone "v1.0.0" --milestone "v1.0.0-rc"
      --released-at "2020-06-30T07:00:00Z"
```

### Notes

- GitLab Release CLI is still under development. Please report any issues in
its project [issue tracker](https://gitlab.com/gitlab-org/release-cli/issues).

- A new pipeline will run when [a new tag is created](https://gitlab.com/gitlab-org/gitlab/issues/16290).
We recommend adding `rules` to your job to prevent these pipelines from
running concurrently with `release-cli`.

### Release asset links

You can generate [release links](https://docs.gitlab.com/ee/user/project/releases/index.html#permanent-links-to-release-assets)
with the `--assets-link` flag, which overrides the values of `--assets-links-name` and `--assets-links-url`. These flags are deprecated and they will be [removed in the future](https://gitlab.com/gitlab-org/release-cli/-/issues/49).

You can pass the value for `--assets-link` multiple times to generate multiple links, such as:

```plaintext
release-cli create --name release-branch-$CI_JOB_ID --description release-branch-$CI_COMMIT_REF_NAME-$CI_JOB_ID
      --tag-name job-$CI_JOB_ID --ref $CI_COMMIT_SHA
      --assets-link '{"name":"Asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy"}'
      --assets-link '{"name":"Asset2","url":"https://<domain>/some/location/2"}'
      --milestone "v1.0.0" --milestone "v1.0.0-rc"
      --released-at "2020-06-30T07:00:00Z"
```

When using `assets`, the [Releases API](https://docs.gitlab.com/ee/api/releases/index.html) only requires `name` and `url`, but the `release-cli` **does not** validate if they are present. If there is an issue with the defined parameters for an asset,
the release will not be created and an API error will occur, for example:

```shell
$ release-cli create \ 
    --name "Release $CI_COMMIT_TAG" \
    --description "Notes: $EXTRA_DESCRIPTION" \
    --tag-name $CI_COMMIT_TAG --ref $CI_COMMIT_SHA \
    --assets-link '{"name":"Asset1","url":"https://<domain>/some/location/1","link_type":"other","filepath":"xzy"}' \
    --assets-link '{"name":"Asset2","url":"https://<domain>/some/location/2"}' --milestone "m1" --milestone "m2" --released-at "2020-08-20T6:42:00Z"

time="2020-08-20T06:48:58Z" level=info msg="Creating Release..." cli=release-cli command=create name="Release v1.9.0-rc" project-id=18239424 ref=56a2713ede44cb567cc26778ae15f11f01a789d5 server-url="https://gitlab.com" tag-name=v1.9.0-rc version="0.3.0~beta.52.g33bbb8d"

time="2020-08-20T06:48:58Z" level=fatal msg="failed to create release: API Error Response status_code: 400 message: Validation failed: Links url is blocked: URI is invalid, Links filepath is invalid" cli=release-cli version="0.3.0~beta.52.g33bbb8d"
```

### Release assets as Generic packages

You can use [Generic packages](https://docs.gitlab.com/ee/user/packages/generic_packages/) to host your release assets. A complete example of how to do this can be found [here](../docs/examples/release-assets-as-generic-package/).

### Use a custom certificate authority

You can specify a custom CA file via the `--additional-ca-cert-bundle` flag or the `$ADDITIONAL_CA_CERT_BUNDLE` environment variable.
See a complete example of how to [use a custom certificate authority](examples/additional-ca-cert-bundle/index.md).
